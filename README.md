# Carnex Admin Api
 
## 项目介绍
Carnex 后台代码

## 环境要求
- PHP 8.0.x or Higher
- Mysql 5.7.x

## 开发环境搭建
以docker为例说明，自建环境请自行研究  
1. 在代码根目录执行
```shell
docker-compose up -d
```
2. 配置正确的`.env`
```shell
cp .env.example .env
```
然后修改以为字段，正确配置`mysql`
```ini
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db name
DB_USERNAME=db user 
DB_PASSWORD=db passwd
```
3. 如果是第一次运行，需要进行数据库初始化，执行以下命令
```shell
docker exec carnex-app chown -R www-data:www-data /var/www/carnex/storage
docker exec carnex-app php /var/www/carnex/artisan migrate
docker exec carnex-app php /var/www/carnex/artisan db:seed
```
## FAQ
1. 建议mysql独立部署，如果确实需要组合部署，请看`docker-compose.yml`的注释
2. 代码默认已经完成`composer install`

# 接口规范
- 后台接口地址：`http://xxx.xxx/admin`
- 前台接口地址：`http://xxx.xxx/api`
## HTTP 状态码解析

|  状态码   | 含义  | 后续动作  |
|  ----  | ----  | ----  |
| 200  | 请求成功 | |
| 401  | 用户没有登录 |跳登录页或弹出登录提醒 |
| 403  | 用户没有操作权限 | |
| 404  | 链接不存在 |跳首页 |
| 405  | 不允许访问 |跳首页 |
| 500  | 系统繁忙 | |

## Code对照表
HTTP状态码为`200`的情况下，系统信息码（返回json里的`code`）对照表 

|  状态码   | 含义  | 提醒信息  |
|  ----  | ----  | ----  |
| 200  | 接口处理成功 | |
| 400  | 接口处理失败 | 可使用message作为提醒信息 |

# 需求列表
1. 基础功能
  - [x] 多语言支持
  - [x] 部署脚本
  - [x] 接口交互规范
  - [x] JWT接口访问认证
  - [x] 升级框架&优化部署方式
2. 后台登录
  - [x] 使用邮箱登录
  - [x] 登录错误提示语：`Incorrect email address or password, please try again.`
  - [x] `state=ban` 禁止登录后台,提示`This account has been banned, please contact the management .`
  - [x] 用户退出
3. Vehicle management
  - [x] 车辆信息同步
    - [x] 不再根据远端车辆管理系统更新状态
    - [x] 同步车辆数据时如车辆uuid为新，则随机Pageviews数5~15，Likes数0~3
    - [ ] 车辆信息固定同步间隔10分钟，新增不存在uuid，更新已存在uuid信息
    - [x] 旧系统为：coming soon：检查本地是否标记手动下架，如是则不再自动上架，如否则自动上架
    - [x] 车辆信息每次同步到本地时远端系统状态的处理：
      - isbooked: true：变更本地车辆状态为sold pending
      - issold: true：变更本地车辆状态为sold
  - [x] 车辆Entity设计
    - [x] `SHOW MAINTENANCE TAB`字段
    - [x] 车辆状态：`initial`，`in stock`，`coming soon`，`selling`，`sold pending`，`In process`，`sold`
    - [x] 车辆信息带视频地址
    - [x] 浏览量
    - [x] like数
  - 车辆列表
    - [x] 筛选
    - [x] 前台
    - [x] 后台
  - 车辆详情
    - [x] 前台
    - [x] 后台
  - [x] 车辆状态变更：允许对没有订单关联或管理订单状态为canceled的车辆进行状态变更操作，否则禁用本下拉框
4. 订单
  - [x] 订单Entity设计
  - [x] 订单列表
    - [ ] VIN筛选搜索
    - [ ] Art No.筛选搜索
    - [ ] order No.筛选搜索
    - [ ] 状态筛选搜索
  - [ ] 后台创建订单（参考修改和详情）
    - [ ] 车辆状态是否允许创建订单
  - [x] 订单详情获取
    - [x] 用户相关信息
    - [x] 支付总计
    - [x] 支付方式
    - [x] 送货信息
    - [x] 驾照
  - [x] 订单基础信息修改
    - [x] 用户相关信息修改
    - [x] 用户支付信息修改
    - [x] 优惠信息保存
    - [x] 送货信息修改
    - [x] 驾照信息修改
    - [x] 记录修改版本或快照
  - [ ] 订单状态变换逻辑
    - [ ] 订单取消
      - [x]用户
      - [x]管理员
      - [ ]远程解锁车辆
  - [ ] 支付状态变换逻辑
    - [ ] 退款
    - [ ] 单独提供支付状态
  - [x] 贷款状态变换逻辑
  - [ ] 订单自动解锁

4. 优惠券
  - [ ] 优惠码数据库设计

5. 后台账号管理
  - [x] 账户Entity设计
    - 状态设计：`Ban`,`Delete`,`Available`
  - [x] 创建账户
  - [x] 账户详情
  - [x] 修改用户信息
    - [x] 修改手机
    - [x] 修改密码,提示：`Please input the same password`
    - [x] 状态变更
  - [ ] 账户角色
    - 创建后为一般权限级别，可使用order和car功能，高级管理权限需修改表来提权
  - [x] 账户列表
    - [x] `name`筛选搜索
    - [x] `email`筛选搜索
    - [x] `phone`筛选搜索
    - [x] `Delete`状态的用户不展示
6. 财务管理
  - [x] 收费信息表设计
  - [x] 收费信息获取（前台/后台）
7. 其他配置
  - [x] 预定信息配置
    - [x] 保存
    - [x] 获取（前台/后台）
  - [x] `Protection Plan`配置
    - [x] 保存
    - [x] 获取（前台/后台）
8. 前台
  - [x] 修改用户信息 