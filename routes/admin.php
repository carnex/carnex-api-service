<?php
/*
|--------------------------------------------------------------------------
| Application Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group([], function () use ($router) {
    // 后端当前版本
    $router->get('version', ['uses' => 'VersionController@current']);
});
/**
 * AuthController 控制器分组
 */
$router->group([], function () use ($router) {
    // 授权登录接口
    $router->post('auth/login', ['uses' => 'AuthController@login']);

    // 退出登录接口
    $router->post('auth/logout', ['uses' => 'AuthController@logout']);

    $router->get('auth/menus', ['uses' => 'AuthController@menus']);
});

/**
 * AccountController 控制器分组
 */
$router->group([], function () use ($router) {
    $router->get('account/detail', ['uses' => 'AccountController@detail']);
    $router->post('account/update-password', ['uses' => 'AccountController@updatePassword']);
    $router->post('account/update-account', ['uses' => 'AccountController@updateAccount']);
});

/**
 * AdminsController 控制器分组
 */
$router->group(['middleware'=>['rbac']], function () use ($router) {
    $router->get('admins/lists', ['uses' => 'AdminsController@lists']);
    $router->post('admin', ['uses' => 'AdminsController@create']);
    $router->put('admin/{id}', ['uses' => 'AdminsController@edit']);
    $router->get('admin/{id}', ['uses' => 'AdminsController@detail']);
    // $router->post('admins/delete', ['uses' => 'AdminsController@delete']);
    // $router->post('admins/update-password', ['uses' => 'AdminsController@updatePassword']);
    // $router->post('admins/update-status', ['uses' => 'AdminsController@updateStatus']);
});

/**
 * RbacController 控制器分组
 */
$router->group([], function () use ($router) {
    // 角色相关接口
    $router->post('rbac/create-role', ['uses' => 'RbacController@createRole']);
    $router->post('rbac/edit-role', ['uses' => 'RbacController@editRole']);
    $router->post('rbac/delete-role', ['uses' => 'RbacController@deleteRole']);
    $router->get('rbac/roles', ['uses' => 'RbacController@roles']);

    // 权限相关接口
    $router->post('rbac/create-permission', ['uses' => 'RbacController@createPermission']);
    $router->post('rbac/edit-permission', ['uses' => 'RbacController@editPermission']);
    $router->post('rbac/delete-permission', ['uses' => 'RbacController@deletePermission']);
    $router->get('rbac/permissions', ['uses' => 'RbacController@permissions']);

    // 分配角色权限
    $router->post('rbac/give-role-permission', ['uses' => 'RbacController@giveRolePermission']);
    $router->post('rbac/give-admin-permission', ['uses' => 'RbacController@giveAdminPermission']);
    $router->get('rbac/get-role-permission', ['uses' => 'RbacController@getRolePerms']);


    $router->get('rbac/get-admin-permission', ['uses' => 'RbacController@getAdminPerms']);
});
/**
 * OrderController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('order', ['middleware' => [], 'uses' => 'OrderController@list']);
    $router->get('order/{id}', ['middleware' => [], 'uses' => 'OrderController@detail']);
    $router->post('order', ['middleware' => [], 'uses' => 'OrderController@create']);
    $router->put('order/{id}', ['middleware' => [], 'uses' => 'OrderController@edit']);
});
/**
 * SystemController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('system/configuration', ['middleware' => [], 'uses' => 'SystemController@all']);
    $router->get('system/configuration/{key}', ['middleware' => [], 'uses' => 'SystemController@detail']);
    $router->post('system/configuration', ['middleware' => [], 'uses' => 'SystemController@update']);
});
/**
 * FeeController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('fee', ['middleware' => [], 'uses' => 'FeeController@all']);
    $router->get('fee/{id}', ['middleware' => [], 'uses' => 'FeeController@detail']);
    $router->post('fee', ['middleware' => [], 'uses' => 'FeeController@create']);
    $router->put('fee/{id}', ['middleware' => [], 'uses' => 'FeeController@update']);
    $router->delete('fee/{id}', ['middleware' => [], 'uses' => 'FeeController@delete']);
});
/**
 * VehicleController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('vehicle/list', ['middleware' => [], 'uses' => 'VehicleController@list']);
    $router->get('vehicle/{id}', ['middleware' => [], 'uses' => 'VehicleController@detail']);
    $router->put('vehicle/{id}', ['middleware' => [], 'uses' => 'VehicleController@update']);
});

