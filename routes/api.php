<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * AuthController 控制器分组
 */
$router->group([], function () use ($router) {
    $router->get('version', ['uses' => 'VersionController@current']);
    // 授权登录接口
    $router->post('auth/login', ['middleware' => [], 'uses' => 'AuthController@login']);

    // 退出登录接口
    $router->get('auth/logout', ['middleware' => [], 'uses' => 'AuthController@logout']);

    // 会员注册接口
    $router->post('auth/register', ['middleware' => [], 'uses' => 'AuthController@register']);
});

/**
 * OrderController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('order', ['middleware' => [], 'uses' => 'OrderController@list']);
    $router->get('order/hold', ['middleware' => [], 'uses' => 'OrderController@holdList']);
    $router->get('order/{id}', ['middleware' => [], 'uses' => 'OrderController@detail']);
    $router->post('order', ['middleware' => [], 'uses' => 'OrderController@create']);
    $router->put('order/{id}', ['middleware' => [], 'uses' => 'OrderController@edit']);
});

/**
 * VehicleController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('vehicle/list', ['middleware' => [], 'uses' => 'VehicleController@list']);
    $router->get('vehicle/{id}', ['middleware' => [], 'uses' => 'VehicleController@detail']);
    $router->post('vehicle/pv/{id}', ['middleware' => [], 'uses' => 'VehicleController@pv']);
    $router->post('vehicle/like/{id}', ['middleware' => [], 'uses' => 'VehicleController@like']);
});
/**
 * SystemController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('system/configuration', ['middleware' => [], 'uses' => 'SystemController@all']);
    $router->get('system/configuration/{key}', ['middleware' => [], 'uses' => 'SystemController@detail']);
});
/**
 * FeeController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('fee', ['middleware' => [], 'uses' => 'FeeController@all']);
    $router->get('fee/{id}', ['middleware' => [], 'uses' => 'FeeController@detail']);
});

/**
 * OrderController 控制器分组
 */
$router->group([], function () use ($router) {
    // 案例接口
    $router->get('user/{email}', ['middleware' => [], 'uses' => 'UserController@detail']);
    $router->post('user/{email}', ['middleware' => [], 'uses' => 'UserController@createOrEdit']);
});