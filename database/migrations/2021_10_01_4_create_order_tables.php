<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateOrderTables extends Migration
{
    private array $tables = [
        "order" => "order",
        "order_extra" => "order_extra",
        "order_edit_log" => "order_edit_log",
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tables["order"], function (Blueprint $table) {
            $table->unsignedBigInteger('id', true)->comment('id');

            $table->string('order_id', 64)->unique('uk_order_id')->default('')->comment('order number');
            $table->string('vin', 100)->default('')->comment('VIN or Art. No.')->index("idx_vin");
            $table->string('vehicle_name', 255)->default('')->comment('车辆名称')->index("idx_vehicle_name");
            $table->string('purchaser', 255)->default('')->comment('purchaser');
            $table->string('email', 255)->default('')->comment('purchaser')->index("idx_mail");
            $table->tinyInteger('is_admin')->default(0)->comment('是否后台建单[0:否;1:是]');
            $table->string('creator', 255)->default('')->comment('创建者')->index("idx_creator");

            $table->boolean("is_delete")->default(false)->comment('是否删除');
            
            $table->tinyInteger('pay_type')->default(0)->comment('支付方式:详看Order Model');
            $table->tinyInteger('status')->default(0)->comment('订单当前状态:详看Order Model');
            $table->tinyInteger('loan_status')->default(0)->comment('贷款当前状态:详看Order Model');
            $table->tinyInteger('pay_status')->default(0)->comment('支付当前状态:详看Order Model');
            $table->tinyInteger('deposit_pay_status')->default(0)->comment('订金支付当前状态:详看Order Model');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });
        Schema::create($this->tables["order_extra"], function (Blueprint $table) {
            $table->unsignedBigInteger('id', true)->comment('id');

            $table->string('order_id', 64)->unique('uk_order_id')->default('')->comment('order number');
            $table->json("extra")->comment("其他信息");

            $table->foreign('order_id')->references('order_id')->on($this->tables["order"]);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });
        Schema::create($this->tables["order_edit_log"], function (Blueprint $table) {
            $table->unsignedBigInteger('id', true)->comment('id');
            $table->string('order_id', 64)->default('')->comment('order number')->index("idx_order_id");
            $table->json("before")->comment("原数据");
            $table->json("after")->comment("改后数据");
            $table->tinyInteger('edit_type')->default(0)->comment('操作类型[0:用户操作;1:后台操作]');
            $table->string('email', 255)->default('')->comment('操作人员');
            $table->foreign('order_id')->references('order_id')->on($this->tables["order"]);
            $table->timestamp('created_at')->useCurrent();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table){
            Schema::dropIfExists($table);
        }
    }
}
