<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->id();
            $table->string('username', 50)->unique("uk_name")->comment('管理员账号');
            $table->string('password', 100)->default('')->comment('登录密码');
            $table->string('email', 50)->unique("uk_mail")->default('')->comment('邮箱');
            $table->string('avatar')->default('')->comment('头像');
            $table->string('nickname')->default('', 30)->comment('昵称');
            $table->string('phone')->default('', 30)->comment('手机号');
            $table->tinyInteger('status')->default(1)->comment('账号状态[0:已禁用;1:正常;]');
            $table->timestamp('last_login_time')->comment('最后登录时间');
            $table->string('last_login_ip', 20)->default('')->comment('最后登录IP');
            $table->tinyInteger('is_delete')->default(0)->comment('是否已删除[0:否;1:是]');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });

        $prefix = DB::getConfig('prefix');
        DB::statement("ALTER TABLE `{$prefix}admin` comment '管理员信息表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
