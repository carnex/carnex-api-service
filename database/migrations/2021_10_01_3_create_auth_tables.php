<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateAuthTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $rolesTable = 'role';
        $roleUserTable = 'role_admin';
        $permissionsTable = 'permission';
        $permissionRoleTable = 'role_permission';
        $adminPermissions = 'admin_permission';

        $userModel = new \App\Models\Admin();
        $userKeyName = $userModel->getKeyName();
        $usersTable = $userModel->getTable();

        Schema::create($rolesTable, function (Blueprint $table) {
            $table->increments('id')->comment('角色ID');
            $table->string('name')->unique()->comment('角色名');
            $table->string('display_name')->nullable()->comment('角色显示名称');
            $table->string('description')->nullable()->comment('角色描述');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });

        Schema::create($roleUserTable, function (Blueprint $table) use ($userKeyName, $rolesTable, $usersTable) {
            $table->increments('id')->comment('自增id');
            $table->bigInteger('admin_id')->unsigned()->comment('管理员用户ID');
            $table->integer('role_id')->unsigned()->comment('角色ID');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();

            $table->foreign('admin_id')->references($userKeyName)->on($usersTable)->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on($rolesTable)->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['admin_id', 'role_id'],"uk_admin_id_role_id");

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });

        Schema::create($permissionsTable, function (Blueprint $table) {
            $table->increments('id')->comment('权限ID');
            $table->unsignedInteger('parent_id')->default(0)->comment('父权限ID');
            $table->tinyInteger('type')->default(0)->comment('权限类型[0:目录;1:菜单;2:权限]');
            $table->string('title')->default('')->comment('权限标题');
            $table->string('path')->default('')->nullable()->comment('权限路由地址');
            $table->string('component')->default('')->comment('前端页面组件名称');
            $table->string('perms')->default('')->comment('权限标识');
            $table->string('icon')->default('')->comment('菜单图标');
            $table->smallInteger('sort')->default(0)->comment('排序[值越小越靠前]');
            $table->tinyInteger('hidden')->default(0)->comment('排序[值越小越靠前]');
            $table->tinyInteger('is_frame')->default(0)->comment('是否外链[0:否;1:是]');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });

        Schema::create($permissionRoleTable, function (Blueprint $table) use ($permissionsTable, $rolesTable) {
            $table->increments('id')->comment('自增id');
            $table->integer('role_id')->unsigned()->comment('角色ID');
            $table->integer('permission_id')->unsigned()->comment('权限ID');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();

            $table->foreign('role_id')->references('id')->on($rolesTable)->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('permission_id')->references('id')->on($permissionsTable)->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['permission_id', 'role_id'],"uk_permission_id_role_id");

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });

        Schema::create($adminPermissions, function (Blueprint $table) use ($permissionsTable) {
            $table->increments('id')->comment('自增id');
            $table->unsignedBigInteger('admin_id')->comment('管理员ID');
            $table->integer('permission_id')->unsigned()->comment('权限ID');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();

            $table->foreign('permission_id',"fk_permission_id")->references('id')->on($permissionsTable)->onDelete('cascade');
            $table->unique(['permission_id', 'admin_id'], 'uk_permission_id_admin_id');
        });

        $prefix = DB::getConfig('prefix');
        DB::statement("ALTER TABLE `{$prefix}{$rolesTable}` comment 'RBAC - 角色表'");
        DB::statement("ALTER TABLE `{$prefix}{$roleUserTable}` comment 'RBAC - 角色、用户关联表'");
        DB::statement("ALTER TABLE `{$prefix}{$permissionsTable}` comment 'RBAC - 权限表'");
        DB::statement("ALTER TABLE `{$prefix}{$permissionRoleTable}` comment 'RBAC - 角色、权限关联表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role');
        Schema::dropIfExists('role_admin');
        Schema::dropIfExists('role_permission');
        Schema::dropIfExists('permission');
        Schema::dropIfExists('admin_permission');
    }
}
