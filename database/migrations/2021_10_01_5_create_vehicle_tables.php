<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateVehicleTables extends Migration
{
    private array $tables = [
        "vehicle" => "vehicle",
        "vehicle_sync_log" => "vehicle_sync_log",
        "vehicle_freed_log" => "vehicle_freed_log",
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tables["vehicle"], function (Blueprint $table) {
            $table->unsignedBigInteger('id', true)->comment('id');

            $table->string('uuid', 100)->unique("uk_vin")->default('')->comment('UUID/VIN');
            $table->string('art_no', 100)->default('')->comment('Art No./stocknumber');
            $table->tinyInteger("status")->default(0)->index("idx_status")->comment('status[0:init,10:已入库,20:待上架/coming soon,30:销售中,40:已预定/sold pending,50:手续中,60:已售出/sold,70:手动下架]');
            $table->tinyInteger("type")->default(0)->comment('类型[0:gas,1:ev]');
            $table->tinyInteger("is_allow_sync")->default(1)->comment('能否自动同步：0不再同步车辆库，1自动同步车辆库，以远程为准');
            $table->string('name', 100)->default('')->comment('车名');
            $table->string('make', 100)->default('')->comment('品牌')->index("idx_make");
            $table->string('model', 100)->default('')->comment('型号')->index("idx_model");
            $table->integer('prodyear')->default(0)->comment('年份')->index("idx_year");
            $table->string('carfax', 255)->default('')->comment('carfax');
            $table->string('currency_unit', 10)->default('CAD')->comment('currency unit');
            $table->decimal('saleprice', $precision = 12, $scale = 2)->comment('vehicle price');
            $table->json("extra")->comment("车辆信息完整json");
            $table->string("extra_md5")->comment("车辆信息完整json字符串的md5码");
            $table->tinyInteger("is_edit")->default(0)->comment('是否本系统修改过信息[0:yes,1:no]');

            $table->integer('pv')->default(0)->comment('page view');
            $table->integer('like')->default(0)->comment('like');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });
        Schema::create($this->tables["vehicle_sync_log"], function (Blueprint $table) {
            $table->unsignedBigInteger('id', true)->comment('id');
            $table->string('host', 255)->default('')->comment('host');
            $table->string('uri', 255)->default('')->comment('uri');
            $table->integer('success_count')->default(0)->comment('请求api成功次数');
            $table->integer('fail_count')->default(0)->comment('请求api失败次数');
            $table->tinyInteger("type")->default(0)->comment('status[0:系统自动运行,1:手动触发]');
            $table->string('email', 255)->default('')->comment('对应手动触发的操作人员');
            $table->json("extra")->comment("成功失败的记录");
            $table->text("app_exception")->comment("报错时的日志");

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });
        Schema::create($this->tables["vehicle_freed_log"], function (Blueprint $table) {
            $table->unsignedBigInteger('id', true)->comment('id');
            $table->string('host', 255)->default('')->comment('host');
            $table->string('uri', 255)->default('')->comment('uri');
            $table->string('uuid', 255)->default('')->comment('uuid');
            $table->integer('http_status_code')->default(0)->comment('http状态码');
            $table->text("request_car")->nullable()->comment("调用接口是通过GET /car?id=xxx 获取到的车辆数据");
            $table->json("local_data")->comment("本地车辆数据");
            $table->text("response")->nullable()->comment("调用接口返回的数据");

            $table->timestamp('created_at')->useCurrent();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table){
            Schema::dropIfExists($table);
        }
    }
}
