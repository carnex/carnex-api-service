<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations. 
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->unsignedInteger('id', true)->comment('user id');
            $table->string('email', 255)->unique('uk_email')->default('')->comment('e-mail');
            $table->string('firstname', 50)->default('')->comment('firstname');
            $table->string('lastname', 50)->default('')->comment('firstname');
            $table->string('phone', 30)->default('')->comment('phone');
            $table->timestamp('birth')->nullable()->comment('birth');
            $table->string('address', 20)->default('')->comment('address');
            $table->string('city', 20)->default('')->comment('city');
            $table->string('province', 20)->default('')->comment('province');
            $table->string('postal', 20)->default('')->comment('postal');
            
            $table->json("extra")->nullable()->comment("不常用信息");
            $table->string('avatar', 255)->default('')->comment('avatar');
            $table->string('password', 100)->default('')->comment('passwd');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
            $table->engine = 'InnoDB';
        });

        $prefix = DB::getConfig('prefix');

        DB::statement("ALTER TABLE `{$prefix}user` comment '会员用户信息表'");
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
