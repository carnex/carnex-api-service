<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['161e6636f2e255b4b714b2cb73c7fd2c', '{"id": 1, "desc": "Finance only", "name": "Documentation Fee 456", "type": 0, "value": 45600, "feeType": 0, "belongType": 1, "remarkType": 0}'],
            ['aa95e3e042573343c94319a1d4054e25', '{"id": 2, "desc": "15%", "name": "HST 15", "type": 1, "ratio": 15, "feeType": 0, "belongType": 0, "remarkType": 1}'],
            ['fbe97978ff86c245dfafe5e648771c4a', '{"id": 3, "desc": "Depsit 72h", "name": "Depsit today", "type": 0, "value": 25000, "feeType": 2, "belongType": 0, "remarkType": 0}'],
        ];

        foreach ($data as $val) {
            DB::table('fee')->insert([
                'k'=>$val[0],
                'extra'=>$val[1],
            ]);
        }
    }
}
