<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->insert([
            'username' => 'carnex',
            'nickname' => 'carnex',
            'phone' => '13800138000',
            'email' => 'admin@carnex.com',
            'password' => Hash::make('Carnex@2021'),
            'status' => Admin::STATUS_ENABLES,
            'last_login_time' => date('Y-m-d H:i:s'),
            'last_login_ip' => '127.0.0.1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('admin')->insert([
            'username' => 'banuser',
            'nickname' => 'banuser',
            'phone' => '13900000',
            'email' => 'banuser@carnex.com',
            'password' => Hash::make('Carnex@2021'),
            'status' => Admin::STATUS_DISABLES,
            'last_login_time' => date('Y-m-d H:i:s'),
            'last_login_ip' => '127.0.0.1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
