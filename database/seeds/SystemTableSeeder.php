<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['coverage', '[{"id": 1, "name": "200,000 km", "price": 89900}, {"id": 2, "name": "400,000 km", "price": 109900}]'],
            ['deposit', '{"reserveTime": 250}'],
            ['gap', '[{"id": 1, "name": "96 month", "price": 124900}, {"id": 2, "name": "84 month", "price": 112900}]'],
            ['protection', '[{"id": 1, "name": "48 months/80,000km", "sort": 4, "price": 214900}, {"id": 2, "name": "36 months/60,000km", "sort": 3, "price": 164900}, {"id": 3, "name": "24 months/40,000km", "sort": 2, "price": 129900}, {"id": 4, "name": "12 months/20,000km", "sort": 1, "price": 114900}]'],
        ];

        foreach ($data as $val) {
            DB::table('system')->insert([
                'k'=>$val[0],
                'v'=>$val[1],
            ]);
        }
    }
}
