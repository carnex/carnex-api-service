<?php
// resources/lang/en/system.php
return [
    '405' => 'The server returned a "405 Method Not Allowed".',
    '404' => 'The server returned a "404 Not Found".',
    'save_success' => 'Saved successfully".',
    'save_fail' => 'Save failed, please try again',
    'id_difference' => '请求id与参数id不一致',
];
