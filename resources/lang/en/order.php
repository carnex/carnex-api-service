<?php
// resources/lang/en/auth.php
return [
    'create_fail' => 'create order fail,please try again',
    'detail_fail' => 'not your order OR no this order',
    'can_not_edit_canceled' => '已取消的订单不能被修改',
    'can_not_edit_deleted' => '已删除的订单不能被修改',
    'can_not_edit_complete' => '已完成的订单不能被修改',
    'admin_create_mail_fail' => '买家邮箱必填，且必须符合邮箱格式',
];
