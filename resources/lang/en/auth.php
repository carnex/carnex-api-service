<?php
// resources/lang/en/auth.php
return [
    'admin_ban' => 'This account has been banned, please contact the management .',
    'login_fail' => 'Incorrect email address or password, please try again.',
    'logout_success' => 'Successfully logged out',
    'no_login' => 'Please login'
];
