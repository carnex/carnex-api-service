<?php
// resources/lang/en/auth.php
return [
    'no_vehicle' => '车辆不存在',
    'status_err' => '当前车辆状态已被锁定，无法售卖，请刷新页面查看车辆最新状态',
    'not_exist' => '车辆不存在',
    'involved_order' => '车辆已被关联到订单，而且订单不是取消状态，不能更改状态',
];
