<?php

namespace App\Listeners;

use App\Events\FreedVehicleEvent;
use App\Repositorys\VehicleRepository;
use Illuminate\Support\Facades\Log;

class FreedVehicleListener
{
    private $vechicleRepository;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(VehicleRepository $vechicleRepository)
    {
        $this->vechicleRepository = $vechicleRepository;
    }

    /**
     * Handle the event.
     *
     * @param FreedVehicleEvent $event
     */
    public function handle(FreedVehicleEvent $event)
    {
        //
        Log::info(sprintf("FreedVehicleEvent:%s:%s",$event->vehicle->uuid,$event->vehicle->status));
        $this->vechicleRepository->freedRemoteVehicle($event->vehicle);
    }
}
