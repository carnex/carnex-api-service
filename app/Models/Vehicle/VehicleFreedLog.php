<?php

namespace App\Models\Vehicle;

use App\Models\BaseModel;

/**
 * Class VehicleFreedLog
 *
 * @property int $id
 * @property int $http_status_code
 * @property string $host
 * @property string $uri
 * @property string $uuid
 * @property string $request_car
 * @property string $local_data
 * @property string $response
 * @property string|null $created_at
 *
 * @package App\Models\VehicleFreedLog
 */
class VehicleFreedLog extends BaseModel
{
    /**
     * @var string 定义表名字
     */
    protected $table = 'vehicle_freed_log';
    
    const UPDATED_AT = null;

}
