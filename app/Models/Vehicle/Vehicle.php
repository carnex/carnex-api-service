<?php

namespace App\Models\Vehicle;

use App\Events\FreedVehicleEvent;
use App\Models\BaseModel;
use Illuminate\Support\Facades\Log;

/**
 * App\Models\Vehicle
 *
 * @property int $id
 * @property string $uuid
 * @property string $art_no
 * @property integer $status
 * @property integer $type
 * @property integer $is_edit
 * @property integer $is_allow_sync
 * @property integer $year
 * @property integer $pv
 * @property integer $like
 * @property double $price
 * @property string $name
 * @property string $make
 * @property string $model
 * @property string $carfax
 * @property string $extra
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @package App\Models\Order
 * @method static find($vin)
 */
class Vehicle extends BaseModel
{
    /**
     * @var string 定义表名字
     */
    protected $table = 'vehicle';
    protected $primaryKey = 'uuid';
    protected $keyType = 'string';

    //车辆类型
    const EV = 1;
    const GAS = 0;

    //状态：[0:起始状态;10:已入库;20:待上架;30:销售中;40:已预定;50:手续中;60:已售出]
    const STATUS_INIT = 0;
    const STATUS_IN_STOCK = 10;
    const STATUS_COMMING_SOON = 20;
    const STATUS_SELLING = 30; //销售中
    const STATUS_SOLD_PENDING = 40;
    const STATUS_IN_PROCESS = 50;
    const STATUS_SOLD = 60;

    public function setStatusAttribute($status)
    {
        $releaseStatus = [self::STATUS_INIT,self::STATUS_IN_STOCK,self::STATUS_COMMING_SOON,self::STATUS_SELLING];
        $status = intval($status);
        $this->attributes['status'] = $status;
        // dd($this->getOriginal("status"));
        // dd($this->getDirty());
        if(in_array($status,$releaseStatus) && !in_array($this->getOriginal("status"),$releaseStatus)){
            //释放远程车辆
            event(new FreedVehicleEvent($this));
        }
    }
}
