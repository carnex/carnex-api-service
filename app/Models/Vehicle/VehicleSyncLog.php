<?php

namespace App\Models\Vehicle;

use App\Models\BaseModel;

/**
 * App\Models\Vehicle
 *
 * @property int $id
 * @property string $host
 * @property string $uri
 * @property integer $success_count
 * @property integer $fail_count
 * @property integer $type
 * @property string $email
 * @property string $extra
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @package App\Models\VehicleSyncLog
 */
class VehicleSyncLog extends BaseModel
{
    /**
     * @var string 定义表名字
     */
    protected $table = 'vehicle_sync_log';

}
