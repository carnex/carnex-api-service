<?php

namespace App\Models\Order;

use App\Models\BaseModel;

/**
 * App\Models\Order\Order
 *
 * @property int $id
 * @property string $order_id
 * @property json $extra
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @package App\Models\Order
 */
class OrderExtra extends BaseModel
{
    /**
     * @var string 定义表名字
     */
    protected $table = 'order_extra';

}
