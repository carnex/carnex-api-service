<?php

namespace App\Models\Order;

use App\Models\BaseModel;

/**
 * App\Models\Order\OrderEditLog
 *
 * @property int $id
 * @property string $order_id
 * @property json $before
 * @property json $after
 * @property int $edit_type
 * @property string $email
 * @property string|null $created_at
 *
 * @package App\Models\OrderEditLog
 */
class OrderEditLog extends BaseModel
{
    /**
     * @var string 定义表名字
     */
    protected $table = 'order_edit_log';

    const ADMIN_EDIT = 1;
    const USER_EDIT = 0;
}
