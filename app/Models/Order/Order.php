<?php

namespace App\Models\Order;

use App\Models\BaseModel;

/**
 * App\Models\Order\Order
 *
 * @property int $id
 * @property string $order_id
 * @property string $vin
 * @property string $vehicle_name
 * @property string $purchaser
 * @property string $email
 * @property string $creator
 * @property boolean $is_admin
 * @property boolean $is_delete
 * @property int $loan_status
 * @property int $pay_status
 * @property int $deposit_pay_status
 * @property int $status
 * @property int $pay_type
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @package App\Models\Order
 */
class Order extends BaseModel
{
    const PROPERTY = ["id","order_id","vin","vehicle_name","purchaser","email","is_admin","creator","is_delete","pay_type","status","loan_status","pay_status","deposit_pay_status","created_at","updated_at"];
    /**
     * @var string 定义表名字
     */
    protected $table = 'order';

    const DELETE = 1;

    //支付方式[0:未知;10:现金;20:贷款]
    const PAY_TYPE_OTHER = 0;
    const PAY_TYPE_CASH = 10;
    const PAY_TYPE_LOAN = 20;

    //订单当前状态[0:起始状态;10:已生成;11:仅预订;20:已预定;30:已确认;40:交付;50:交易完成;60:取消中;70:已取消]
    const STATUS_INIT = 0;
    const STATUS_CREATED = 10;
    const STATUS_HOLD = 11;
    const STATUS_BOOKED = 20;
    const STATUS_ACCEPTED = 30;
    const STATUS_DELIVER = 40;
    const STATUS_COMPLETE = 50;
    const STATUS_CANCELING = 60;
    const STATUS_CANCELED = 70;

    //贷款状态：[0:起始状态;10:待提交;20:待审核;30:待补充;40:审核成功;50:审核失败;60:已取消]
    const LOAN_INIT = 0;
    const LOAN_WAIT_SUBMIT = 10;
    const LOAN_WAIT_AUDIT = 20;
    const LOAN_WAIT_ADDITION = 30;
    const LOAN_SUCCESS = 40;
    const LOAN_FAIL = 50;
    const LOAN_CANCELED = 60;

    //订金支付状态：[0:起始状态;10:已生成;20:成功;30:失败;40:取消中;50:已取消;]
    const DEPOSIT_PAY_INIT = 0;
    const DEPOSIT_PAY_CREATED = 10;
    const DEPOSIT_PAY_SUCCESS = 20;
    const DEPOSIT_PAY_FIAL = 30;
    const DEPOSIT_PAY_CANCELING = 40;
    const DEPOSIT_PAY_CANCELED = 50;

    //支付状态：pay_status[0:未支付;10:已支付;20:已退款]
    const PAY_UNPAY = 0;
    const PAY_PAID = 10;
    const PAY_REFUND = 20;

}
