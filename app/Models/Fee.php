<?php

namespace App\Models;
/**
 * Class System
 *
 * @property int $id
 * @property string $k
 * @property string $extra
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @package App\Models
 * @method static find($k)
 */
class Fee extends BaseModel
{
    /**
     * @var string 定义表名字
     */
    protected $table = 'fee';
}
