<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User Model
 *
 * @property integer $id 用户ID
 * @property string $email 用户昵称
 * @property string $firstname 用户昵称
 * @property string $lastname 用户昵称
 * @property string $phone 用户昵称
 * @property string $birth 用户昵称
 * @property string $address 用户昵称
 * @property string $city 用户昵称
 * @property string $province 用户昵称
 * @property string $postal 登录手机号
 * @property string $password 登录密码
 * @property string $avatar 头像
 * @property string $extra 性别
 * @property string $created_at 注册时间
 *
 * @package App\Models
 */
class User extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    
    /**
     * @var string 定义表名字
     */
    protected $table = 'user';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * 设置密码
     *
     * @param string $value 密码值
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
