<?php

namespace App\Models;
/**
 * Class System
 *
 * @property string $k
 * @property string $v
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @package App\Models
 * @method static find($k)
 */
class System extends BaseModel
{
    /**
     * @var string 定义表名字
     */
    protected $table = 'system';
    protected $primaryKey = 'k';
    public $incrementing = false;
    protected $keyType = 'string';
}
