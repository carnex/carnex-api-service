<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return Response
     */
    public function handle($request, Closure $next)
    {
        if (!$request->headers->has('Origin')) {
            return $next($request);
        }

        $headers = [];
        if(!env("NGINX_CORS")){
            $headers = [
                'Access-Control-Allow-Credentials' => 'true',
                'Access-Control-Max-Age' => '86400',
                'Access-Control-Allow-Origin' => '* always',
                'Access-Control-Allow-Methods' => 'OPTIONS,GET,POST,PUT,DELTE,PATCH',
                'Access-Control-Allow-Headers' => 'X-Requested-With,Content-Type,Authorization,token,EMAIL,Origin, Accept,ceping-session,kjxl-session,auth-token',
            ];
        }
        if ($request->isMethod('OPTIONS')) {
            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }

        $response = $next($request);
        foreach ($headers as $key => $value) {
            $response->header($key, $value);
        }

        return $response;
    }
}
