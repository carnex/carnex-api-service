<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\OrderException;
use App\Exceptions\ResponseCode;
use App\Exceptions\VehicleException;
use App\Models\Order\Order;
use App\Services\OrderService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

use function PHPUnit\Framework\isNull;

class OrderController extends CController
{
    /**
     * 用户服务层
     *
     * @var OrderService
     */
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * 生成订单
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {

        $data = $request->input();
        $mail = $data['buyer']['email'] ?? "";
        if(empty($mail) || !filter_var($mail,FILTER_VALIDATE_EMAIL)){
            return $this->fail(__("order.admin_create_mail_fail"));
        }
        $order = new Order();
        $order->vin = $data['vehicle']['uuid'];
        $order->vehicle_name = $data['vehicle']['name'];
        $order->purchaser = $data['basicInfo']['firstname']." ".$data['basicInfo']['lastname'];
        $order->email = $mail;
        $order->is_admin = 1;
        $order->creator = $this->user()->email;
        $order->status = $request->input("status");
        try {
            $order_id = $this->orderService->create($order,$data);
            if($order_id){
                return $this->success([
                    'order_id' => $order_id
                ]);
            }
        }catch (VehicleException $e){
            return $this->fail($e->getMessage());
        }
        return $this->fail(__("order.create_fail"));
    }
    /**
     * 获取所有订单
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function list(Request $request)
    {
        $where = [];
        if(!is_null($request->input("status")) && $request->input("status") != ""){
            $where[] = ["status",intval($request->input("status"))];
        }
        $page = $request->input("page") ?? 1;
        $size = $request->input("page_size") ?? 20;
        $data = $this->orderService->getOrderList($where,$size,$page);
        return $this->success($data);
    }
    public function detail(Request $request,string $id): JsonResponse
    {
        $order = $this->orderService->getByOrderId($id);
        if(!empty($order)){
            return $this->success($order);
        }
        return $this->fail(__("order.detail_fail"));
    }
    public function edit(Request $request,string $id){
        try{
            $save = $this->orderService->edit($request,$id,$this->user(),true);
        }catch(OrderException $e){
            return $this->fail($e->getMessage());
        }
        if($save){
            return $this->success([
                'order_id' => $id
            ]);
        }
        return $this->fail(__("system.save_fail"));
    }
}
