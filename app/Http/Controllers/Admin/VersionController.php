<?php

namespace App\Http\Controllers\Admin;

class VersionController extends CController
{
    public function current(){
        return $this->success(["app" => "admin"],"version 1.0");
    }
}
