<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminsController extends CController
{
    /**
     * 获取管理员列表
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function lists(Request $request)
    {
        $this->validate($request, [
            'page' => 'required|integer:min:1',
            'page_size' => 'required|in:10,20,30,50,100',
            'status' => 'in:0,1',
        ]);

        $result = services()->adminService->getAdmins($request);
        return $this->success($result);
    }

    public function detail(Request $request,int $id)
    {
        $user = Admin::find($id);
        if(!empty($user)){
            unset($user->password);
        }
        return $this->success($user);
    }
    /**
     * 添加管理员账号
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        /**
         * {
                "email":"aaa@carnet.com",
                "name":"aaa",
                "passwd":"123456",
                "phone":"13800138000",
            }
         */
        $this->validate($request, [
            'email' => 'required',
            'passwd' => 'required',
        ]);
        try {
            $result = services()->adminService->create($request);

        }catch (AdminException $e){
            return $this->fail($e->getMessage());
        }
        if (!$result) {
            return $this->fail(__("system.save_fail"));
        }

        return $this->success([], __("system.save_success"));
    }
    /**
     * 修改账号信息
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function edit(Request $request,int $id)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required',
            'is_delete' => 'required'
        ]);
        if($request->input('id') != $id){
            return $this->fail('账号id与提交id不一致，修改失败');
        }
        $result = services()->adminService->edit($request);
        if (!$result) {
            return $this->fail('管理员账号修改失败...');
        }

        return $this->success([], '管理员账号修改成功...');
    }
}
