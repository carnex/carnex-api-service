<?php

namespace App\Http\Controllers\Admin;

use App\Models\Vehicle\Vehicle;
use App\Services\OrderService;
use App\Services\VehicleService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use stdClass;

class VehicleController extends CController
{
    /**
     * 用户服务层
     *
     * @var VehicleService
     */
    protected $vehicleService;
    /**
     * @var OrderService
     */
    protected $orderService;

    public function __construct(VehicleService $vehicleService,OrderService $orderService)
    {
        $this->vehicleService = $vehicleService;
        $this->orderService = $orderService;
    }
    /**
     * 获取所有车辆
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $all = $this->vehicleService->getAdminAll($request->input());
        $count = $this->vehicleService->adminCount($request->input());
        return $this->success([
            "content" => $all,
            "index" => intval($request->input("page")),
            "pages" => ceil($count/$request->input("size")),
            "total" => $count,
        ],"ok");
    }
    public function detail(Request $request,string $id): JsonResponse
    {
        $vehicle = $this->vehicleService->getByVin($id);
        if($vehicle){
            return $this->success(json_decode($vehicle->extra));
        }
        return $this->fail(__("vehicle.not_exist"));
    }
    public function update(Request $request,string $id){
        $this->validate($request, [
            'uuid' => 'required',
            'status' => 'required',
        ]);
        if($id != $request->input("uuid")){
            return $this->fail(__("system.id_difference"));
        }
        $vehicle = Vehicle::find($id);
        if(empty($vehicle)){
            return $this->fail(__("vehicle.not_exist"));
        }
        $input = $request->input();
        $vehicle->type = Vehicle::GAS;
        $vehicle->is_edit = 0;
        $db_extra = json_decode($vehicle->extra);
        if(isset($input['ev']) && !empty($input["ev"])){
            $vehicle->type = Vehicle::EV;
        }
        foreach($input as $k => $v){
            if(!isset($db_extra->$k)){
                $vehicle->is_edit = 1;
            }
            $db_extra->$k = $v;
        }
        //车辆状态变更：允许对没有订单关联或管理订单状态为canceled:71的车辆进行状态变更操作，否则不允许变更
        if($vehicle->status != $input["status"]){
            // if($this->orderService->hasInvolvedVehicle($id)){
            //     //存在不是取消的订单，关联了这车，不能更改状态
            //     return $this->fail(__("vehicle.involved_order"));
            // }
            $vehicle->status = $input["status"];
        }
        $vehicle->extra = json_encode($db_extra);
        $vehicle->extra_md5 = md5($vehicle->extra);
        $vehicle->updated_at = Carbon::now();
        try{
            $save = $vehicle->save();
            if($save){
                return $this->success((new stdClass()),__("system.save_success"));
            }
        }catch(Exception $e){
            
        }
        return $this->fail(__("system.save_fail"));
    }
}
