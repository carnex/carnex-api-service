<?php

namespace App\Http\Controllers\Admin;

use App\Models\Fee;
use App\Services\FeeService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use stdClass;

class FeeController extends CController
{
    protected FeeService $feeService;
    public function __construct(FeeService $feeService)
    {
        $this->feeService = $feeService;
    }

    public function detail(Request $request,int $id){
        $fee = Fee::find($id);
        if(empty($fee)){
            $fee = new stdClass();
        }else{
            $t = json_decode($fee->extra);
            $t->id = $fee->id;
            $fee = $t;
        }
        return $this->success($fee);
    }
    public function all(Request $request){

        return $this->success($this->feeService->getAll());
    }
    public function update(Request $request,int $id){
        $fee = Fee::find($id);
        if(empty($fee)){
            return $this->fail(__("fee.not_exist"));
        }
        $data = json_decode($fee->extra);
        $name = $request->input("name") ?? $data->name;
        $feeTyep = $request->input("feeType") ?? $data->feeType;
        $remarkType = $request->input("remarkType") ?? $data->remarkType;
        $key = md5($name.'-'.$feeTyep.'-'.$remarkType);
        $input = $request->input();
        foreach($input as $k => $v){
            $data->$k = $v;
        }
        $fee->k = $key;
        if(empty($data)){
            $fee->extra = new stdClass();
        }else{
            $fee->extra = json_encode($data);
        }
        $fee->updated_at = Carbon::now();
        try{
            $save = $fee->save();
            if($save){
                return $this->success((new \stdClass()),__("system.save_success"));
            }
        }catch(Exception $e){

        }
        return $this->fail(__("system.save_fail"));
    }
    public function create(Request $request){
        try{
            $key = $request->input("name")."-".$request->input("feeType")."-".$request->input("remarkType");
            $key = md5($key);
            $fee = Fee::where("k",$key)->first();
            if(!empty($fee)){
                return $this->fail(__("fee.exist_key"));
            }
            $fee = new Fee();
            $fee->k = $key;
            if(empty($request->input())){
                $fee->extra = new stdClass();

            }else{
                $fee->extra = json_encode($request->input());
            }
            $save = $fee->save();
            if($save){
                return $this->success((new \stdClass()),__("system.save_success"));
            }
        }catch(Exception $e){
            
        }
        return $this->fail(__("system.save_fail"));
    }
    public function delete(Request $request,int $id){
        $fee = Fee::find($id);
        if(empty($fee)){
            return $this->success((new \stdClass()),__("system.save_success"));
        }
        $del = $fee->delete();
        if($del){
            return $this->success((new \stdClass()),__("system.save_success"));
        }
        return $this->fail(__("system.save_fail"));
    }
}
