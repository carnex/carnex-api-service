<?php

namespace App\Http\Controllers\Admin;


use App\Services\SystemService;
use Illuminate\Http\Request;

class SystemController extends CController
{
    protected SystemService $systemService;
    public function __construct(SystemService $systemService)
    {
        $this->systemService = $systemService;
    }

    public function detail(Request $request,string $key){
        return $this->success($this->systemService->get($key));
    }
    public function all(Request $request){

        return $this->success($this->systemService->get());
    }
    public function update(Request $request){
        $rs = $this->systemService->update($request->input());
        if($rs){
            return $this->success((new \stdClass()),__("system.save_success"));
        }
        return $this->fail(__("system.save_fail"));
    }

}
