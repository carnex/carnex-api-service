<?php

namespace App\Http\Controllers\Api;

use App\Models\Vehicle\Vehicle;
use App\Services\VehicleService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use stdClass;

class VehicleController extends CController
{
    /**
     * 用户服务层
     *
     * @var VehicleService
     */
    protected $vehicleService;

    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }
    /**
     * 获取所有车辆
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $all = $this->vehicleService->getAll($request->input());
        $count = $this->vehicleService->count($request->input());
        return $this->success([
            "content" => $all,
            "index" => intval($request->input("page")),
            "pages" => ceil($count/$request->input("size")),
            "total" => $count,
        ],"ok");
    }
    public function detail(Request $request,string $id): JsonResponse
    {
        $vehicle = $this->vehicleService->getByVin($id);
        if($vehicle){
            $extra = json_decode($vehicle->extra);
            $extra->status = $vehicle->status;
            $extra->pv = $vehicle->pv;
            $extra->like = $vehicle->like;

            return $this->success($extra);
        }
        return $this->fail(__("order.detail_fail"));
    }
    public function pv(Request $request,string $id){
        if(empty($id)){
            $id = "";
        }
        $vehicle = Vehicle::find($id);
        if($vehicle instanceof Vehicle){
            $vehicle->pv += 1;
            $vehicle->save();
        }
        return $this->success((new stdClass()));
    }
    public function like(Request $request,string $id){
        if(empty($id)){
            $id = "";
        }
        $vehicle = Vehicle::find($id);
        if($vehicle instanceof Vehicle){
            $vehicle->like += 1;
            $vehicle->save();
        }
        return $this->success((new stdClass()));
    }
}
