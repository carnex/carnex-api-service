<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ResponseCode;
use App\Exceptions\VehicleException;
use App\Models\Order\Order;
use App\Models\User;
use App\Services\OrderService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use stdClass;

class UserController extends CController
{
    public function __construct()
    {
        // 授权中间件
        $this->middleware("auth:{$this->guard}", []);
    }
    public function detail(Request $request,string $email): JsonResponse
    {
        if($this->user()->email != $email){
            //不是本人，返回空结果
            return $this->success((new stdClass()),"not your self");
        }
        $user = User::where("email",$email)->first();
        if(empty($user)){
            $user = new stdClass();
        }
        return $this->success($user);
    }
    public function createOrEdit(Request $request,string $email): JsonResponse
    {
        if($request->input("email") != $email){
            return $this->fail(__("system.id_difference"));
        }
        if($this->user()->email != $email){
            return $this->fail(__("system.save_fail"));
        }
        $user = User::where("email",$email)->first();
        $input = $request->input();
        if(empty($user)){
            //create
            $user = new User();
            foreach($input as $k => $v){
                $user->$k = $v;
            }
        }else{
            //edit
            foreach($input as $k => $v){
                $user->$k = $v;
            }
        }
        try{
            if(empty($user->birth)){
                $user->birth = null;
            }
            $user->save();
        }catch(\Exception $e){
            return $this->fail(__("system.save_fail"));
        }
        return $this->success((new stdClass()),__("system.save_success"));
    }
}
