<?php

namespace App\Http\Controllers\Api;

use App\Models\Fee;
use App\Services\FeeService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use stdClass;

class FeeController extends CController
{
    protected FeeService $feeService;
    public function __construct(FeeService $feeService)
    {
        $this->feeService = $feeService;
    }

    public function detail(Request $request,int $id){
        $fee = Fee::find($id);
        if(empty($fee)){
            $fee = new stdClass();
        }else{
            $t = json_decode($fee->extra);
            $t->id = $fee->id;
            $fee = $t;
        }
        return $this->success($fee);
    }
    public function all(Request $request){

        return $this->success($this->feeService->getAll());
    }
}
