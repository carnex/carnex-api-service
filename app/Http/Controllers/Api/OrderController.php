<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\OrderException;
use App\Exceptions\ResponseCode;
use App\Exceptions\VehicleException;
use App\Models\Order\Order;
use App\Services\OrderService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class OrderController extends CController
{
    /**
     * 用户服务层
     *
     * @var OrderService
     */
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;

        // 授权中间件
        $this->middleware("auth:{$this->guard}", []);
    }

    /**
     * 生成订单
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        //todo check json scheme
        $data = $request->input();
        $order = new Order();
        $order->vin = $data['vehicle']['uuid'];
        $order->vehicle_name = $data['vehicle']['name'];
        $order->purchaser = ($data['basicInfo']['firstname'] ?? "")." ".($data['basicInfo']['lastname'] ?? "");
        $order->email = $this->user()->email;
        $order->creator = $this->user()->email;
        $order->status = $request->input("status");
        if(isset($data["hold"]['orderid'])){
            $order->order_id = $data["hold"]['orderid'];
        }
        try {
            $order_id = $this->orderService->create($order,$data);
            if($order_id){
                return $this->success([
                    'order_id' => $order_id
                ]);
            }
        }catch (VehicleException $e){
            return $this->fail($e->getMessage());
        }
        return $this->fail(__("order.create_fail"));
    }
    /**
     * 获取所有订单
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function list(Request $request)
    {
        $data = $this->orderService->getOrderList([
            ["email",Auth::user()->email],
            ["status","!=",Order::STATUS_HOLD],
        ]);
        return $this->success($data);
    }
    /**
     * 获取所有订单
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function holdList(Request $request)
    {
        $data = $this->orderService->getHoldOrderList($request);
        return response()->json([
            "code" => 0,
            "msg" => "success",
            "result" => true,
            "data" => [
                "content" => $data,
                "index" => 0,
                "pages" => 1
            ]
        ], 200);
    }
    public function detail(Request $request,string $id): JsonResponse
    {
        $order = $this->orderService->getByOrderId($id);
        if(empty($order)){
            return $this->fail(__("order.detail_fail"));
        }
        if($order->email == Auth::user()->email){
            return $this->success($order);
        }
        return $this->fail(__("order.detail_fail"));
    }
    public function edit(Request $request,string $id){
        try{
            $save = $this->orderService->edit($request,$id,Auth::user());
        }catch(OrderException $e){
            return $this->fail($e->getMessage());
        }
        if($save){
            return $this->success([
                'order_id' => $id
            ]);
        }
        return $this->fail(__("system.save_fail"));
    }
}
