<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

class VersionController extends CController
{
    public function current(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->success(["app" => "www"],"version 1.0");
    }
}
