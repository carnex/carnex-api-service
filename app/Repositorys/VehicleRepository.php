<?php

namespace App\Repositorys;

use App\Models\Vehicle\Vehicle;
use App\Models\Vehicle\VehicleFreedLog;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

/**
 * Class OrderRepository
 *
 * @package App\Repositorys
 */
class VehicleRepository
{
    public function getByVin($vin): Vehicle|null
    {
        return Vehicle::find($vin);
    }
    public function create(Vehicle $vehicle): bool
    {
        return $vehicle->save();
    }
    public function getAll($condition = []){
        $db = $this->conditionHandler($condition,Vehicle::select("extra","status","pv","like"));
        return $db->get();
    }
    public function getCount($condition = []){
        $db = $this->conditionHandler($condition,Vehicle::select("id"),true);
        return $db->count();
    }
    private function conditionHandler($condition,$db,$is_count = false){
        $page = $condition["page"] ?? 0;
        $size = $condition["size"] ?? 12;
        $page = intval($page);
        $size = intval($size);

        $name = $condition["name"] ?? "";
        $make = $condition["make"] ?? "[]";
        $model = $condition["model"] ?? "[]";
        $sorts = $condition["sorts"] ?? "[]";
        $year = $condition["year"] ?? "[]";
        $price = $condition["price"] ?? "[]";
        
        $make = $this->stringArrayToArray($make);
        $model = $this->stringArrayToArray($model);
        $sorts = $this->stringArrayToArray($sorts);
        $year = $this->stringArrayToArray($year);
        $price = $this->stringArrayToArray($price);
        
        
        if(!empty($name)){
            $db->where('name','LIKE','%'.$name.'%');
        }
        if(!empty($make)){
            $db->whereIn("make",$make);
        }
        if(!empty($model)){
            $db->whereIn("model",$model);
        }
        if(!empty($year)){
            foreach($year as &$y){
                $y = intval($y);
            }
            $db->whereIn("prodyear",$year);
        }
        if(!empty($price) && count($price) == 2){
            $first = current($price);
            $end = end($price);
            $db->whereBetween("saleprice",[intval($first),intval($end)]);
        }
        if($is_count === false){
            if(!empty($sorts) && count($sorts) == 2){
                $first = current($sorts);
                $end = end($sorts);
                $db->orderBy($first,$end);
            }
            $db->offset($page*$size)->limit($size);
        }
        return $db;
    }
    private function stringArrayToArray($string){
        $string = str_replace(";","",$string);
        $string = trim($string,"[]");
        if(empty($string)){
            return [];
        }
        $arr = explode(",",$string);
        $rs = [];
        foreach($arr as $v){
            $t = str_replace("\"","",$v);
            if(!empty($t)){
                $rs[] = $t;
            }
        }
        return $rs;
    }
    public function getAdminAll($condition = []){
        $db = $this->adminConditionHandler($condition,Vehicle::select("extra","status"));
        return $db->get();
    }
    public function getAdminCount($condition = []){
        $db = $this->adminConditionHandler($condition,Vehicle::select("id"),true);
        return $db->count();
    }
    private function adminConditionHandler($condition,$db,$is_count = false){
        // dd($condition);
        $page = $condition["page"] ?? 0;
        $size = $condition["size"] ?? 12;
        $page = intval($page);
        $size = intval($size);

        $key = $condition["key"] ?? "";
        $status = $condition["status"] ?? -1;
        $status = intval($status);
        
        
        if(!empty($key)){
            $db->where('uuid',$key);
            $db->orWhere('art_no',$key);
            $db->orWhere('make',$key);
        }
        if($status >= 0){
            $db->where('status',$status);
        }
        if($is_count === false){
            if(!empty($sorts) && count($sorts) == 2){
                $first = current($sorts);
                $end = end($sorts);
                $db->orderBy($first,$end);
            }
            $db->offset($page*$size)->limit($size);
        }
        return $db;
    }
    /**
     * @param Vehicle $vehicle 
     */
    public function freedRemoteVehicle($vehicle){
        $host = env("CNX_BACKEND_PUB_URL","https://carnexapi.azurewebsites.net");

        $login = Http::post($host."/user/login",[
            "email" => env("CNX_BACKEND_EMAIL"),
            "password" => env("CNX_BACKEND_PWD"),
            "accessToken" => env("CNX_BACKEND_TOKEN"),
            "type" => env("CNX_BACKEND_TYPE"),
        ]);
        if($login->status() != 200){
            Log::error("carnex backend login fail :".$host."/user/login");
            return;
        }
        $body = $login->json();
        if(!isset($body['data']) || empty($body['data'])){
            Log::error("carnex backend login fail :".$host."/user/login".$login->body());
            return;
        }
        $token = $body['data'];

        $car = Http::withHeaders(['token' => $token,])->get($host."/car",["id" => $vehicle->uuid]);
        if($car->status() != 200){
            Log::error("carnex backend get car fail :".$host."/car?id=".$vehicle->uuid);
            return;
        }
        $change = Http::withHeaders(['token' => $token,])->post($host."/car/revertsold",$car->json()["data"]);
        $log = new VehicleFreedLog();
        $log->http_status_code = $change->status();
        $log->host = $host;
        $log->uri = "/car/revertsold";
        $log->uuid = $vehicle->uuid;
        $log->request_car = $car->body();
        $log->local_data = json_encode($vehicle);
        $log->response = $change->body();
        try{
            $log->save();
        }catch(Exception $e){
            //todo
        }
        if($change->status() != 200){
            Log::error("carnex backend car revertsold fail :".$host."/car/revertsold");
        }
    }
}
