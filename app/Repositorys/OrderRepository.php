<?php

namespace App\Repositorys;

use App\Models\Order\Order;
use App\Models\Order\OrderExtra;
use App\Models\Vehicle\Vehicle;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

/**
 * Class OrderRepository
 *
 * @package App\Repositorys
 */
class OrderRepository
{
    public function create(Order $order, OrderExtra $orderExtra,Vehicle $vehicle): bool
    {
        $save = false;
        try {
            DB::beginTransaction();
            $order->save();
            $orderExtra->save();
            $vehicle->status = 60;
            $vehicle->save();
            DB::commit();
            $save = true;
        }catch (\Exception $e){
//            print_r($vehicle);
//            print_r($e->getTraceAsString());
            DB::rollBack();
        }
        return $save;
    }
    public function getAll($where = [],$size=50,$current_page = 1){
        $query = Order::leftJoin('order_extra', 'order.order_id', '=', 'order_extra.order_id')
            ->select("order.order_id","order_extra.extra","order.email","order.status","order.pay_type","order.loan_status","order.deposit_pay_status","order.pay_status","order.created_at","order.updated_at");
        if(is_array($where) && !empty($where)){
            foreach($where as $v){
                $query = $query->where(...$v);
            }
        }
        $data = $query->paginate($size,"[*]","page",$current_page)->toArray();
        return $data;
    }
    public function getByOrderId($order_id){
        return DB::table('order_extra')
            ->leftJoin('order', 'order.order_id', '=', 'order_extra.order_id')
            ->where('order_extra.order_id', $order_id)
            ->select("order_extra.extra","order.*")
            ->first();
    }
    public function getHoldList($user){
        if(!isset($user->email) || empty($user->email)){
            return [];
        }
        //hold https://carnexapi.azurewebsites.net//order/list?page=0&size=10&type=0
        /**
        {
            "code": 0,
            "data": {
                "content": [{
                    "bookdate": 1635516858000,
                    "carid": "WDDSJ4GB2JN690493",
                    "carimg": "https://carnexcdn.azureedge.net/public/WDDSJ4GB2JN690493/small.png",
                    "carname": "2018 Mercedes CLA250",
                    "isbooked": false,
                    "ishandled": false,
                    "issold": false,
                    "orderid": "0e7c182b-85c1-462e-b597-b522dcf3daa0",
                    "paymenttoken": "pi_3JpvqUJwv58HY3aF1qeTvGbT_secret_nrUrIAUp5cqV5ssMRLd5gMgpN",
                    "userid": "zhiqinyigu@gmail.com"
                }],
                "index": 0,
                "pages": 1
            },
            "msg": "success",
            "result": true
        }
         */
        $url = env("VEHICLE_HOST","https://carnexapi.azurewebsites.net")."/order/list";
        $remote = Http::withHeaders([
            "token" => $user->token
        ])->get($url,[
            "page" => 0,
            "size" => 1000,
            "type" => 0,
        ]);
        // echo $remote;exit;
        $remote = json_decode($remote);
        // dd($remote);
        
        $list = [];
        if(isset($remote->code) && $remote->code == 0){
            //有数据
            $t = $remote->data->content ?? [];
            foreach($t as $v){
                $list[$v->carid] = $v;
            }
        }
        $orders = Order::select("order.order_id","order.vin","order.status","order_extra.extra")
            ->leftJoin('order_extra', 'order.order_id', '=', 'order_extra.order_id')
            ->where('order.email', $user->email)->get();
        // dd($list);
        if(empty($orders)){
            return array_values($list);
        }
        foreach($orders as $order){
            if($order->status == Order::STATUS_CANCELED){
                continue;
            }
            if(isset($list[$order->vin])){
                if($order->status != Order::STATUS_HOLD){
                    unset($list[$order->vin]);
                }else{
                    $list[$order->vin]->order_id = $order->order_id;
                }
            }
        }
        return array_values($list);
    }
}
