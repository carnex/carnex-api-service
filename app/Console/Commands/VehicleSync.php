<?php

namespace App\Console\Commands;

use App\Models\Vehicle\Vehicle;
use App\Models\Vehicle\VehicleSyncLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class VehicleSync extends Command
{
    /**
     * 命令名称及签名
     *
     * @var string
     */
    protected $signature = 'vehicle:sync';

    /**
     * 命令描述
     *
     * @var string
     */
    protected $description = 'sync vehicle list';

    /**
     * 创建命令
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * 执行命令
     *
     */
    /**
     *   - [ ] 车辆信息同步
    - [ ] 不再根据远端车辆管理系统更新状态can_sync
    - [ ] 同步车辆数据时如车辆uuid为新，则随机Pageviews数5~15，Likes数0~3
    - [ ] 车辆信息固定同步间隔10分钟，新增不存在uuid，更新已存在uuid信息
    - [ ] 旧系统为：coming soon（inspectdate:true）：检查本地是否标记手动下架，如是则不再自动上架，如否则自动上架
    - [ ] 车辆信息每次同步到本地时远端系统状态的处理：
    - isbooked: true：变更本地车辆状态为sold pending
    - issold: true：变更本地车辆状态为sold
     * 'status[0:init,10:已入库,20:待上架/coming soon,30:销售中,40:已预定/sold pending,50:手续中,60:已售出/sold,70:手动下架]'
     */
    public function handle()
    {
        echo "sync vehicle\r\n";
        $success_count = 0;
        $fail_count = 0;
        $remote_cars = [];
        $logs = [
            "success" => [],
            "fail" => [],
        ];
        $host = env("VEHICLE_HOST","https://carnexapi.azurewebsites.net");
        // $uri = "/car/list?size=30&name=&make=&model=&sorts=&year=&price=&page=0";//调试用，只拿一条数据
        $uri = "/car/list?size=10000&name=&make=&model=&sorts=&year=&price=&page=0";//一次过把所有数据拿回来


        $get = Http::get($host.$uri);
        $data = json_decode($get->body());
        if($data->code != 0){
            //获取失败
            $fail_count++;
            $logs['fail'][] = $uri;
        }else{
            $success_count++;
            $logs['success'][] = $uri;
        }
        foreach ($data->data->content as $car){
            $remote_cars[$car->uuid] = $car;
        }
        //处理同步回来的数据
        $db_data = Vehicle::query()->select(["uuid","status","is_allow_sync","extra_md5","is_edit"])->get();
//        print_r($db_data);
        $update_cars = [];
        $is_edit_cars = [];
        foreach ($db_data as $v){
            if(!isset($remote_cars[$v->uuid])){
                //本地数据库存在这个车，但远程没有，跳过处理
                continue;
            }
            //本地存在这个车，根据实际情况看是否更新
            $remote_car = $remote_cars[$v->uuid];
            unset($remote_cars[$v->uuid]);
            // if(!$v->is_allow_sync){
            //     //不再同步远程
            //     continue;
            // }
            // if(in_array($v->status,[Vehicle::STATUS_SOLD_PENDING,Vehicle::STATUS_IN_PROCESS,Vehicle::STATUS_SOLD])){
            //     //如果本地已经预定，手续中，售卖后，不再更新
            //     continue;
            // }
            // if ($v->status == 70){
            //     //手动下架，如是则不再自动上架，如否则自动上架：不在改变状态，只变更信息
            //     $remote_car->status = $v->status;
            //     $update_cars[] = $remote_car;
            //     continue;
            // }
            if($v->is_edit){
                //是否本系统修改过信息[0:yes,1:no]
                $is_edit_cars[$v->uuid] = 1;
            }
            if($v->extra_md5 != md5(json_encode($remote_car))){
                //判断远程车辆情况，如果状态和信息有改变，走数据更新
                $update_cars[] = $remote_car;
            }
            
        }
//        print_r($update_cars);
//        print_r($remote_cars);
//        exit;
        DB::beginTransaction();
        try {
            if(!empty($update_cars)){
                //更新
                foreach ($update_cars as $car){
                    //需要处理额外字段问题，ev,maintenance
                    $up_field = $this->varsHandler($car);
                    // print_r($up_field);
                    if(isset($is_edit_cars[$up_field["uuid"]]) && isset($up_field["var_list"]['extra'])){
                        $db_vehicle = Vehicle::where("uuid",$up_field["uuid"])->select("extra")->first();
                        $db_extra = json_decode($db_vehicle->extra);
                        $var_extra = json_decode($up_field["var_list"]['extra']);
                        // print_r($var_extra);
                        foreach($db_extra as $exk => $exv){
                            if(isset($var_extra->$exk)){
                                $db_extra->$exk = $var_extra->$exk;
                            }
                        }
                        // if(isset($db_extra->ev)){
                        //     $var_extra->ev = $db_extra->ev;
                        // }
                        // if(isset($db_extra->maintenance)){
                        //     $var_extra->maintenance = $db_extra->maintenance;
                        // }
                        $up_field["var_list"]['extra'] = json_encode($db_extra);
                    }
                    Vehicle::where("uuid",$up_field["uuid"])->update($up_field["var_list"]);
                }
            }
            if(!empty($remote_cars)){
                //远程存在，本地没有的车，走新增
                foreach ($remote_cars as $car){
                    $up_field = $this->varsHandler($car);
                    $up_field["var_list"]["uuid"] = $up_field["uuid"];
                    //同步车辆数据时如车辆uuid为新，则随机Pageviews数5~15，Likes数0~3
                    $up_field["var_list"]["pv"] = rand(5,15);
                    $up_field["var_list"]["like"] = rand(0,3);
//                    print_r($up_field);
                    Vehicle::create($up_field["var_list"]);
                }
            }
            //保存log
            VehicleSyncLog::create([
                "host" => $host,
                "uri" => $uri,
                "success_count" => $success_count,
                "fail_count" => $fail_count,
                "extra" => json_encode($logs),
                "app_exception" => ""
            ]);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            VehicleSyncLog::create([
                "host" => $host,
                "uri" => $uri,
                "success_count" => $success_count,
                "fail_count" => $fail_count,
                "extra" => json_encode($logs),
                "app_exception" => $e->getTraceAsString()
            ]);
        }
    }
    private function varsHandler($obj)
    {
        /**
        [content] => Array
        (
            [0] => stdClass Object
            (
                [bodytype] => Truck
                [carfaxlink] => https://vhr.carfax.ca/?id=37E%2bwZPqPagkH%2fj9Uhpfx9vKS%2fzPlH8L
                [conditions] => USED
                [description] => The Toyota Tacoma is renowned as one of the all-time greatest pick-up trucks. This model being outfitted with the TRD Sport package offers everything you will need on your Monday-to-Friday commute, as well as those off-roading weekend trips.
                [discount] => 0
                [doors] => 4
                [drivetrain] => AWD
                [engine] => 3.5 L V6
                [exteriorcolours] => 24,52,7
                [fueltype] => 12.0L/100KM
                [hightlightfeature] => ["Heated Seats", "Back-Up Camera", "Solid Fold Bed Cover", "4x4 Lock", "Keyless Entry", "Push-Button Start", "Power Windows", "Power Mirrors", "Heated Mirrors", "Home-Link System", "400W Charging"]
                [horsepower] => 278
                [imgurl] => https://carfaxcanadabadgingcdn.azureedge.net/content/images/v3/en/Logo_NoBadges.svg
                [inspectdate] => false
                [isbooked] =>
                [issold] =>
                [make] => Toyota
                [mdspprice] => 0
                [mileage] => 92526
                [model] => Tacoma
                [name] => 2017 Toyota Tacoma
                [owner] => 3
                [printedkeys] => 2
                [prodyear] => 2017
                [saleprice] => 39199
                [stocknumber] => C14892
                [torque] => 265
                [transmission] => AUTO
                [trim] => TRD Sport
                [uuid] => 5TFDZ5BNXHX014892
                [vrlink] => https://spins.spincar.com/youngleasing/c14892
                [warranty] => Started as of September 16th 2016: Powertrain Warranty 60 MONTHS/100,000 km.
            )

        )
         */
        $status = Vehicle::STATUS_SELLING;//销售中
        if(isset($obj->status)){
            //已经存在状态，使用存在的
            $status = $obj->status;
            unset($obj->status);
        }else{
            //新车或可变状态数据，使用远程数据覆盖
            $inspectdate = $obj->inspectdate ?? false;
            $issold = $obj->issold ?? false;
            $isbooked = $obj->isbooked ?? false;

            if($this->boolHandler($issold)){
                $status = Vehicle::STATUS_SOLD;//已售卖
            }elseif ($this->boolHandler($isbooked)){
                $status = Vehicle::STATUS_SOLD_PENDING;//已预订
            }elseif ($this->boolHandler($inspectdate)){
                $status = Vehicle::STATUS_COMMING_SOON;//coming soon
            }
        }
        $md5 = md5(json_encode($obj));
        //把车的状态增加进去
        $obj->status = $status;
        return [
            "uuid" => $obj->uuid,
            "var_list" => [
                "art_no" => $obj->stocknumber,
                "status" => $status,
                "name" => $obj->name,
                "make" => $obj->make,
                "model" => $obj->model,
                "prodyear" => intval($obj->prodyear),
                "carfax" => $obj->carfaxlink ?? "",
                "saleprice" => $obj->saleprice ?? 0,
                "extra" => json_encode($obj),
                "extra_md5" => $md5,
            ]
        ];
    }
    private function boolHandler($source){
        if(is_bool($source)){
            return $source;
        }
        if(is_string($source)){
            if($source === "true"){
                return true;
            }
            return false;
        }
        return boolval($source);
    }
}
