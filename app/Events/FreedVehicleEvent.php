<?php

namespace App\Events;

use App\Models\Vehicle\Vehicle;

class FreedVehicleEvent extends Event
{
    public $vehicle;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
