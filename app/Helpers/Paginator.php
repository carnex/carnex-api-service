<?php
namespace App\Helpers;

use Illuminate\Pagination\LengthAwarePaginator;

class Paginator extends LengthAwarePaginator
{
    public function toArray()
    {
        return [
            'content' => $this->items->toArray(),
            'total' => $this->total(),
            'pages' => $this->lastPage(),
            'index' => $this->currentPage(),
        ];
    }
}
