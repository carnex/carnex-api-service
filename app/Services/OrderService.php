<?php

namespace App\Services;

use App\Exceptions\OrderException;
use App\Exceptions\VehicleException;
use App\Models\Order\Order;
use App\Models\Order\OrderEditLog;
use App\Models\Order\OrderExtra;
use App\Repositorys\OrderRepository;
use App\Repositorys\VehicleRepository;
use Illuminate\Support\Str;
use App\Models\Vehicle\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
 * 用户服务类
 *
 * Class OrderService
 * @package App\Services
 */
class OrderService
{
    protected OrderRepository $orderRepository;
    protected VehicleRepository $vehicleRepository;
    public function __construct(OrderRepository $orderRepository,VehicleRepository $vehicleRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->vehicleRepository = $vehicleRepository;
    }

    /**
     * @throws VehicleException
     */
    public function create(Order $order, $data): string|bool
    {
        //检查车辆状态是否可用于创建订单
        $vehicle = $this->vehicleRepository->getByVin($order->vin);
        if(empty($vehicle)){
            //vehicle is not exist
            throw new VehicleException(__("vehicle.no_vehicle"));
        }
        if ($vehicle->status != Vehicle::STATUS_SELLING){
            throw new VehicleException(__("vehicle.status_err"));
        }
        if(empty($order->order_id)){
            $order->order_id = Str::uuid()->toString();
        }

        $orderExtra = new OrderExtra();
        $orderExtra->order_id = $order->order_id;
        foreach($data as $k => $v){
            //清理主表字段
            if(in_array($k,Order::PROPERTY)){
                unset($data[$k]);
            }
        }
        $orderExtra->extra = json_encode($data);

        $vehicle->uuid = $order->vin;
        //hold单处理
        if($order->status == Order::STATUS_HOLD){
            $order->deposit_pay_status == Order::DEPOSIT_PAY_SUCCESS;
        }else{
            $order->status = Order::STATUS_CREATED;
        }
        //车辆改已预定
        $vehicle->status = Vehicle::STATUS_SOLD_PENDING;
        $save = $this->orderRepository->create($order,$orderExtra,$vehicle);
        if($save){
            return $order->order_id;
        }
        return false;
    }
    public function getOrderList($where = [],$size=50,$current_page = 1){
        $data = $this->orderRepository->getAll($where,$size,$current_page);
        $rs = [];
        if(empty($data['content'])){
            return $data;
        }
        foreach ($data['content'] as $v){
            $t = json_decode($v["extra"]);
            $t->id = $v["order_id"];
            $t->status = $v['status'];
            $t->pay_type = $v['pay_type'];
            $t->loan_status = $v['loan_status'];
            $t->deposit_pay_status = $v['deposit_pay_status'];
            // $t->pay_status = $v['pay_status'];
            $t->email = $v['email'];
            $t->created_at = $v['created_at'];
            $t->updated_at = $v['updated_at'];
            $rs[] = $t;
        }
        $data['content'] = $rs;
        return $data;
    }
    public function getByOrderId($order_id){
        $data = $this->orderRepository->getByOrderId($order_id);
        $rs = null;
        if(!empty($data)){
            $rs = json_decode($data->extra);
            $rs->id = $order_id;
            $rs->status = $data->status;
            $rs->pay_type = $data->pay_type;
            $rs->loan_status = $data->loan_status;
            $rs->deposit_pay_status = $data->deposit_pay_status;
            // $rs->pay_status = $data->pay_status;
            $rs->email = $data->email;
            $rs->created_at = $data->created_at;
            $rs->updated_at = $data->updated_at;
        }
        return $rs;
    }
    /**
     * true:有关联
     * false:无关联
     */
    public function hasInvolvedVehicle($vin){
        $data = Order::select("id")->where("vin",$vin)->where("status","!=",Order::STATUS_CANCELED)->get();
        return $data->isNotEmpty();
    }
    public function getHoldOrderList(Request $request){
        $data = $this->orderRepository->getHoldList(Auth::user());
        return $data;
    }
    private function checkBeforeEdit(Order $order){
        //如果是取消单，删除单，不做任何操作
        if($order->is_delete == Order::DELETE){
            throw new OrderException(__("order.can_not_edit_deleted"));
        }
        if($order->status == Order::STATUS_CANCELED){
            throw new OrderException(__("order.can_not_edit_canceled"));
        }
        if($order->status == Order::STATUS_COMPLETE){
            throw new OrderException(__("order.can_not_edit_complete"));
        }
    }
    public function edit(Request $request,string $id,$user,$is_amdin = false){
        //
        $order_id = $request->input("id");
        if($order_id != $id){
            throw new OrderException(__("system.id_difference"));
        }
        $data = $request->input();
        $order = Order::where("order_id",$id)->first();
        if(empty($order)){
            throw new OrderException(__("order.detail_fail"));
        }
        if($is_amdin === false){
            //非管理员修改，判断是否用户的订单
            if($order->email != $user->email){
                throw new OrderException(__("order.detail_fail"));
            }
        }
        //执行修改前，判断订单可操作状态
        $this->checkBeforeEdit($order);

        $beforeData = json_encode($this->orderRepository->getByOrderId($id)); //修改前数据
        
        $vehicle = $this->vehicleRepository->getByVin($order->vin);
        if(is_null($vehicle)){
            $vehicle = new stdClass();
        }
        $orderExtra = OrderExtra::where("order_id",$id)->first();
        foreach($data as $k => $v){
            //清理主表字段
            if(in_array($k,Order::PROPERTY)){
                unset($data[$k]);
            }
        }
        //extra数据处理 start
        $extra = json_decode($orderExtra->extra);
        // dd($data);
        foreach($data as $k => $v){
            if(isset($extra->$k)){
                //如果元素全部为string的，可以单个字段更新，不然就只能全量覆盖
                $can_update_field = true;
                foreach($extra->$k as $vk => $vv){
                    if(!is_string($vv)){
                        $can_update_field = false;
                        break;
                    }
                }
                if($can_update_field){
                    foreach($extra->$k as $vk => $vv){
                        $extra->$k->$vk = $vv;
                    }
                }
            }else{
                $extra->$k = $v;
            }
        }
        $orderExtra->extra = json_encode($extra);
        //extra数据处理 end

        //订单状态变动处理
        $loan_status = intval($request->input("loan_status"));
        if(!empty($loan_status)){
            $order->loan_status = $loan_status;
        }
        $deposit_pay_status = $request->input("deposit_pay_status");
        if(!is_null($deposit_pay_status)){
            $order->deposit_pay_status = intval($deposit_pay_status);
        }
        $status = intval($request->input("status"));
        if(!empty($status)){
            $order->status = $status;
            if($order->status === Order::STATUS_CANCELED){
                //取消订单，释放车辆
                $vehicle->status = Vehicle::STATUS_SELLING;
            }
            if($order->status === Order::STATUS_COMPLETE){
                //完成这个单，车辆卖出
                $vehicle->status = Vehicle::STATUS_SOLD;
            }
        }
        //删除订单
        $is_delete = intval($request->input("is_delete"));
        if($is_delete == Order::DELETE){
            $order->is_delete = $is_delete; //删单
            $order->status = Order::STATUS_CANCELED; //取消单
            $vehicle->status = Vehicle::STATUS_SELLING;
        }
        $save = false;
        try {
            DB::beginTransaction();
            $order->save();
            $orderExtra->save();
            if($vehicle instanceof Vehicle){
                $vehicle->save();
            }
            //log 处理 start
            $afterData = json_encode($this->orderRepository->getByOrderId($id));//重启获取订单数据
            $log = new OrderEditLog();
            $log->order_id = $id;
            $log->before = $beforeData;
            $log->after = $afterData;
            $log->edit_type = intval($is_amdin);
            $log->email = $user->email;
            $log->save();
            //log 处理 end
            DB::commit();
            $save = true;
        }catch (\Exception $e){
            DB::rollBack();
        }
        return $save;
    }
}
