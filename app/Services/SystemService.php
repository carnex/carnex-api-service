<?php

namespace App\Services;


use App\Models\System;
use App\Repositorys\VehicleRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * 用户服务类
 *
 * Class VehicleService
 * @package App\Services
 */
class SystemService
{
    public function get($key = "")
    {
        if(empty($key)){
            $vars = System::all();
            $value = [];
            if(!empty($vars)){
                foreach ($vars as $v){
                    $value[$v->k] = json_decode($v->v);
                }
            }
            return $value;
        }
        $vars = System::find($key);
        $value = new \stdClass();
        if(isset($vars->v)){
            $value = json_decode($vars->v);
        }
        return $value;
    }
    public function update($data){
        $save = true;
        if(!empty($data)){
            DB::beginTransaction();
            try {
                foreach ($data as $k => $v){
                    $system = System::find($k);
                    if(empty($system)){
                        $system = new System();
                        $system->updated_at = Carbon::now();
                    }
                    $system->k = $k;
                    $system->v = json_encode($v);
                    $system->save();
                }
                DB::commit();
            }catch (\Exception $e){
                DB::rollBack();
                $save = false;
            }

        }
        return $save;
    }
}
