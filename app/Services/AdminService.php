<?php

namespace App\Services;

use App\Exceptions\AdminException;
use Carbon\Carbon;
use Exception;
use App\Models\Admin;
use App\Repositorys\AdminRepository;
use App\Traits\PagingTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminService
{
    use PagingTrait;

    /**
     * @var AdminRepository
     */
    protected $adminRepository;

    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    /**
     * 登录业务处理
     *
     * @param array $params
     * @return bool|Admin
     */
    public function login(array $params)
    {
        // 通过登录名查询用户信息
        $admin = $this->adminRepository->findByUserNameOrMail($params['username']);
        if (!$admin) {
            return false;
        }

        // 判断账号是否已被删除
        if (Admin::YES_DELETE == $admin->is_delete) {
            return Admin::BAN;
        }

        // 登录密码验证
        if (!check_password($params['password'], $admin->password)) {
            return false;
        }

        // 判断用户状态
        if ($admin->status !== Admin::STATUS_ENABLES) {
            return Admin::BAN;
        }

        return $admin;
    }
/**
     * 获取管理员列表
     *
     * @param Request $request
     * @return mixed
     */
    public function getAdmins(Request $request)
    {
        $params = [];

        $orderBy = $request->only(['sortField', 'sortOrder']);
        if (isset($orderBy['sortField'], $orderBy['sortOrder'])) {
            $params['order_by'] = $orderBy['sortField'];
            $params['sort'] = get_orderby_sort($orderBy['sortOrder']);
        }

        if ($username = $request->input('key', '')) {
            $params['key'] = addslashes($username);
        }

        if ($status = $request->input('status', '')) {
            if (in_array($status, [1, 2])) {
                $arr = [
                    '1' => Admin::STATUS_ENABLES,
                    '2' => Admin::STATUS_DISABLES,
                ];

                $params['status'] = $arr[$status];
            }
        }

        return $this->adminRepository->findAllAdmins(
            $request->input('page', 1),
            $request->input('page_size', 10),
            $params
        );
    }

    /**
     * 创建管理员账号
     *
     * @param Request $request
     * @return bool
     * @throws AdminException
     */
    public function create(Request $request)
    {
        $check = Admin::where("email",$request->input('email'))->first();
        if(!empty($check)){
            throw new AdminException(__("admin.email_exist"));
        }
        try {
            $admin = new Admin();
            $admin->username = md5($request->input('email'));
            $admin->password = $request->input('passwd');
            $admin->phone = $request->input('phone') ?? "";
            $admin->email = $request->input('email') ?? "";
            $admin->nickname = $request->input('name') ?? "";
            $admin->last_login_time = Carbon::now();
            $admin->status = Admin::STATUS_ENABLES;
            $admin->save();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * 修改账号
     *
     * @param Request $request
     * @return bool
     */
    public function edit(Request $request)
    {
        $admin = Admin::find($request->input('id'));
        if(empty($admin)){
            return false;
        }
        try {
            $update = $request->input();
            foreach ($update as $k => $v){
                $admin->$k = $v;
            }
            $admin->updated_at = Carbon::now();
            $admin->last_login_time = $request->input('last_login_time') ?? Carbon::now();
            return $admin->save();
        } catch (Exception $e) {
            return false;
        }
    }

}
