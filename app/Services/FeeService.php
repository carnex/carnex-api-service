<?php

namespace App\Services;


use App\Models\Fee;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * 用户服务类
 *
 * Class FeeService
 * @package App\Services
 */
class FeeService
{
    public function getAll()
    {
        $all = Fee::all();
        $rs = [];
        foreach($all as $v){
            $t = json_decode($v->extra);
            $t->id = $v->id;
            $rs[] = $t;
        }
        return $rs;
    }
}
