<?php

namespace App\Services;


use App\Models\Vehicle\Vehicle;
use App\Repositorys\VehicleRepository;

/**
 * 用户服务类
 *
 * Class VehicleService
 * @package App\Services
 */
class VehicleService
{
    protected VehicleRepository $vehicleRepository;
    public function __construct(VehicleRepository $vehicleRepository)
    {
        $this->vehicleRepository = $vehicleRepository;
    }

    public function create(Vehicle $vehicle): string|bool
    {
        return $this->vehicleRepository->create($vehicle);
    }
    public function getByVin(string $vin){
        return $this->vehicleRepository->getByVin($vin);
    }
    public function getAll($condition = []){
        $data = $this->vehicleRepository->getAll($condition);
        $rs = [];
        foreach ($data as $v){
            $tmp = json_decode($v["extra"]);
            $tmp->status = $v["status"];
            $tmp->pv = $v["pv"];
            $tmp->like = $v["like"];
            $rs[] = $tmp;
        }
        return $rs;
    }
    public function count($condition = []){
        return $this->vehicleRepository->getCount($condition);
    }
    public function getAdminAll($condition = []){
        $data = $this->vehicleRepository->getAdminAll($condition);
        $rs = [];
        foreach ($data as $v){
            $tmp = json_decode($v["extra"]);
            $tmp->status = $v["status"];
            $rs[] = $tmp;
        }
        return $rs;
    }
    public function adminCount($condition = []){
        return $this->vehicleRepository->getAdminCount($condition);
    }
}
