<?php
// @formatter:off

namespace PHPSTORM_META {

    /**
     * PhpStorm Meta file, to provide autocomplete information for PhpStorm
     *
     * @author Barry vd. Heuvel <barryvdh@gmail.com>
     * @see https://github.com/barryvdh/laravel-ide-helper
     */
    override(\app(0), map([
        'services' => \App\Services\Service::class,
        'request' => \Illuminate\Http\Request::class
    ]));
}
